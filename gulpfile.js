var gulp        = require('gulp'),
    del         = require('del'),
    imagemin    = require('gulp-imagemin'),
    sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    jshint      = require('gulp-jshint'),
    stylish     = require('jshint-stylish'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    browserSync = require('browser-sync').create();


// Define origin paths
var srcPaths = {
    data:     'src/data/',
    images:   'src/img/',
    scripts:  'src/js/',
    styles:   'src/scss/',
    vendor:   'src/js/vendor/',
    root:     'src/'
};


// Define destination paths
var distPaths = {
    data:     'dist/data/',
    images:   'dist/img/',
    scripts:  'dist/js/',
    styles:   'dist/css/',
    vendor:   'dist/js/vendor/',
    root:     'dist/'
};


// Clean all data from dist directory
gulp.task('clean', function(cb) {
    del([
        distPaths.data     + '*.json',
        distPaths.images   + '*.jpg',
        distPaths.scripts  + '*.js',
        distPaths.scripts  + 'maps/*.map',
        distPaths.styles   + '*.css',
        distPaths.styles   + 'maps/*.map',
        distPaths.vendor   + 'js/*.js',
        distPaths.root     + '*.html'
    ], cb);
});


// Copy directly files that don't need processing
gulp.task('copy', ['copyHtml', 'copyVendor', 'copyData'], function() {
    // Empty body, later will probably require browserSynch
});


// Copy html files in root directory
gulp.task('copyHtml', function () {
    return gulp.src([srcPaths.root + '*.html'])
        .pipe(gulp.dest(distPaths.root))
        .pipe(browserSync.stream());
});


// Copy all vendor files
gulp.task('copyVendor', function () {
    return gulp.src([srcPaths.vendor + '**/*'])
        .pipe(gulp.dest(distPaths.vendor))
        .pipe(browserSync.stream());
});


// Copy all data files
gulp.task('copyData', function () {
    return gulp.src([srcPaths.data + '**/*'])
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
});


// Minimize images and copy them to dist folder
gulp.task('imagemin', function() {
    return gulp.src([srcPaths.images + '**/*'])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});


/*
 * Process base.scss file. Options for the output are
 *
 *     nested, expanded, compact, compressed
 *
 * View more on https://github.com/sass/node-sass#options
 *
 * Generate source maps at the same time
 *
 *     https://github.com/floridoo/gulp-sourcemaps
 *
 */
gulp.task('scss', function() {
    return gulp.src([srcPaths.styles + '**/main.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('maps/'))
        .pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream());
});


/*
 * Process js files to detect errors, should be done before using
 * js-concat and js-min
 *
 *    https://www.npmjs.com/package/gulp-jshint
 *
 */
gulp.task('lint', function() {
    return gulp.src([
        srcPaths.scripts + '*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});


/*
 * Process all source JS files and generates a single minified
 * JS file and source maps.
 */
gulp.task('js', ['lint'], function() {
    return gulp.src([
        // srcPaths.scripts + 'contact.js',
        srcPaths.scripts + 'plugins.js',
        srcPaths.scripts + 'main.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream());
});

/*
 * Launch server and add file watchers.
 *
 * This task depends on several others copy, scss, js and imagemin,
 * the last one only in the scope of the exercise since the application
 * does not use the generated images.
 *
 * The task uses the local server through a proxy and defines the
 * browsers that should be launched if they are installed on the system.
 */
gulp.task('serve', ['copy', 'scss', 'js', 'imagemin'], function() {
    browserSync.init({
        logLevel: "info",
        // The original browser names do not work on my install, change the next line
        //      browser: ["google chrome", "Firefox"]
        browser: ["firefox"],       // This works on Ubuntu
        proxy: "localhost:80",
        startPath: "/limingclimbing/",
    });

    gulp.watch(srcPaths.root    + '*.html'       , ['copyHtml']   );
    gulp.watch(srcPaths.vendor  + '**/*'         , ['copyVendor'] );
    gulp.watch(srcPaths.data    + '**/*'         , ['copyData']   );
    gulp.watch(srcPaths.images  + '**/*'         , ['imagemin']   );
    gulp.watch(srcPaths.styles  + '**/*.scss'    , ['scss']       );    // If any scss file changes > reload
    gulp.watch(srcPaths.scripts + '**/*.js'      , ['js']         );
});


// Default task
gulp.task('default', ['clean', 'serve'], function() {});
